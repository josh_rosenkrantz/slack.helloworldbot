const ConversationWrapper = require("./utils/ConversationWrapper");
const ConversationAdapter = require("./utils/ConversationAdapter");
const yeoman = require('yeoman-environment');
const stringify = require('./utils/stringify');


module.exports = (robot) => {
    robot.hear(/hello/i, (msg) => {
        msg.reply("Hello, What can I do for you today? tip:ask help for a list of commands.");
    });

    robot.hear(/create (.*)/i, async (msg) => {
        const conversation = new ConversationWrapper(robot, msg);
        const adapter = new ConversationAdapter(conversation);

        const yo = yeoman.createEnv(null, null, adapter);
        yo.register(require.resolve('generator-helloworld'), 'helloworld');
        yo.run('helloworld');
    });

    robot.respond(/help/i,(msg)=>{
        msg.reply("Currently the only command is: create");
    })


}
const Dialog = require('./Dialog');

const ConversationType = {
    USER: 'user',
    ROOM: 'room'
};

class Conversation {
    constructor(bot, type, listener) {
        this.bot = bot;
        this.type = type || ConversationType.USER;
        this.listener = listener || (() => true);

        this._dialogWith = {};

        this._startListening();
    }

    startDialog(msg, timeout, timeoutMessage) {
        const id = this._getId(msg.message);
        
        const dialog = this._dialogWith[id] = new Dialog(msg, timeout, timeoutMessage);
        dialog.on('timeout', ()=> {
            delete this._dialogWith[id];
        });
        return dialog;
    }

    _startListening() {
        this.bot.listen(async (msg) => {
            const id = this._getId(msg);
            const listen = this.listener(msg);
            return  listen ? this._dialogWith[id] : false;
        }, async (msg) => {
            msg.reply(`is done? => ${msg.done}`);
            const id = this._getId(msg.message);
            await this._dialogWith[id].receive(msg);
        });

    }

    _getId(msg) {
        return (this.type === ConversationType.USER) ? msg.user.id : msg.room;
    }
}

module.exports = {
    Conversation,
    ConversationType
}
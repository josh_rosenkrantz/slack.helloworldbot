const c = require('./constants');
const EventEmitter = require('events').EventEmitter;

const stringify = require('../stringify')

module.exports = class Dialog extends EventEmitter {
    constructor(startingMessage, timeoutValue, timeoutMessage) {
        super();

        this.startingMessage = startingMessage;
        this.timeoutMessage = timeoutMessage || c.DEFAULT_TIMEOUT_MESSAGE;
        this.timeoutValue = timeoutValue || c.DEFAULT_TIMEOUT;
        this._resetChoices();
        this._startTimeout();
    }

    async receive(msg) {
        const text = msg.message.text;
        const promises = this.choices.map(async (choice) => {
            let match = text.match(choice.regex);
            if(match !== null) {
                this._endDialog();

                msg.match = match;
                await choice.handler(msg);
                return true;
            }
            return false;
        });

        const matched = Promise.all(promises);
        if(!matched) {
            this._endDialog();
        }
    }

    addChoice(regex, handler) {
        this.choices.push({
            regex: regex,
            handler: handler
        });
        this._resetTimeout();
    }

    _endDialog() {
        this._resetChoices();
        clearTimeout(this.expiration);
    }

    _resetChoices() {
        this.choices = [];
    }

    _onTimeout(msg) {
        msg.reply(this.timeoutMessage);
    }

    _resetTimeout() {
        clearTimeout(this.expiration);
        this._startTimeout();
    }

    _startTimeout() {
        this.expiration = setTimeout(() => {
            this._onTimeout(this.startingMessage);
            this.emit('timeout', this.startingMessage);
        }, this.timeoutValue);
    }
}
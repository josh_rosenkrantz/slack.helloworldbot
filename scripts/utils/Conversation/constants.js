'use strict';
module.exports = {
    DEFAULT_TIMEOUT_MESSAGE: 'Conversation ended, I never heard back from you.',
    DEFAULT_TIMEOUT: 30000
}
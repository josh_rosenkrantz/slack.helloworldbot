const {Conversation} = require('./Conversation');//hubot-conversation');
const stringify = require('./stringify');

module.exports = class ConversationWrapper {
    constructor(bot, msg) {
        this.bot = bot;
        this.msg = msg;
        this.conversation = new Conversation(this.bot);
        this.answers = { };

        // Finish the message at the start of the conversation
        // this fixes an issue where the the first question is answered by the first command.
        this.msg.finish();
    }

    /// Ask a question, the question to be asked should be in inquirer.js format:
    /// https://github.com/SBoudrias/Inquirer.js#questions
    askQuestion(question) {
        return new Promise((resolve, reject) => {
            const choices = typeof question.choices === "function" ? question.choices() : question.choices;
            let message = question.message;

            if(choices) {
                message = `${message} ${stringify(choices).replace('[', '(').replace(']',')')}`;
            }
            this.msg.reply(message);
            
            let dialog = this.conversation.startDialog(this.msg);
            dialog.addChoice(/cancel/i, (answer) => {
                reject("Conversation cancelled.");
            });

            if(choices) {
                choices.forEach(element => {
                    const r = new RegExp(element, "i");
                    dialog.addChoice(r, (answer) => {
                        this.answers[question.name] = answer.match[0];
                        resolve(this.answers);
                    })
                });
            }
            else {
                dialog.addChoice(/.*/i, (answer) => {
                    this.msg.reply(`answered`)
                    this.answers[question.name] = answer.match[0];
                    resolve(this.answers);
                });
            }
        });
    }
}
/// Adapter for the Yeoman Environment into a conversation.
const logger = require("yeoman-environment/lib/util/log");

module.exports = class ConversationAdapter {
    constructor(conversation) {
        this.log = logger();

        this.conversation = conversation;
    }

    prompt(questions, callback) {
        let promise = questions.reduce((promise, question) => {
            return promise.then((result) => {
                return this.conversation.askQuestion(question);
            });
        }, Promise.resolve());

        if(callback) {
            promise.then(callback);
        }

        promise.catch((reason) => this.log(`ended => ${reason}`));
        return promise;
    }

    diff(actual, expected, changes) {}
}